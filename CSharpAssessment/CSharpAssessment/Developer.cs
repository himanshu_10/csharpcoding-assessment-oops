﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssessment
{
    public abstract class Developer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime JoiningDate { get; private set; }

        public string ProjectAssigned { get; set; }

        public abstract decimal CalculateSalary();

        public Developer(int id, string name, DateTime jDate, string projAssigned)
        {
            Id = id;
            Name = name;
            JoiningDate = jDate;
            ProjectAssigned = projAssigned;
        }
    }
}
