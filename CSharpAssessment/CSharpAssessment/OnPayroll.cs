﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssessment
{
    public class OnPayroll : Developer
    {
        public string Department { get; set; }
        public string Manager { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal Da { get; set; }
        public decimal Hra { get; set; }
        public decimal Lta { get; set; }
        public decimal Pf { get; set; }

        public OnPayroll(int id, string name, DateTime jDate, string projAssigned, string department, string manager, decimal basicSalary, decimal da, decimal hra, decimal lta, decimal pf) : base(id, name, jDate, projAssigned)
        {
            Department = department;
            Manager = manager;
            BasicSalary = basicSalary;
            Da = da;
            Hra = hra;
            Lta = lta;
            Pf = pf;
        }

        public override decimal CalculateSalary()
        {
            return BasicSalary + Da + Hra + Lta - Pf;
        }

        public override string ToString()
        {
            return $"ID: {Id}\nName: {Name}\nDate of Joining : {JoiningDate}\nProject Assigned : {ProjectAssigned}\n Type: On Payroll\nDepartment: {Department}\tManager: {Manager}\nBasic Salary: {BasicSalary}\nDA: {Da}\nHRA: {Hra}\nLTA: {Lta}\nPF: {Pf}\tNet Salary: {CalculateSalary()}";
        }

    }
}
