﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace CSharpAssessment
{
   
   

    class Program
    {
        static List<Developer> developers = new List<Developer>();

        static void Main(string[] args)
        {
            try
            {
                while (true)
                {
                    Console.WriteLine("Menu:");
                    Console.WriteLine("1. Create Developer");
                    Console.WriteLine("2. Calculate Total Salary/Charges");
                    Console.WriteLine("3. Perform LINQ Operationis..");
                    Console.WriteLine("4. Exit");

                    Console.Write("Enter your choice: ");
                    int choice = Convert.ToInt32(Console.ReadLine());
                    if ( choice < 1 || choice > 4)
                    {
                        throw new Exception("Invalid choice.");
                    }

                    switch (choice)
                    {
                        case 1:
                            CreateDeveloper();
                            break;
                        case 2:
                            CalculateTotalSalaryOrCharges();
                            break;
                        case 3:
                            LinqOperations();
                            break;
                        case 4:
                            Environment.Exit(0);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void LinqOperations()
        {

            Console.WriteLine("Performing LINQ Operations...");

            // Display all records
            Console.WriteLine("Displaying All the records");
            Console.WriteLine("\n");
            var list1 = (from x in developers select x).ToList();
            foreach (Developer d in list1)
                Console.WriteLine(d);
            Console.WriteLine("\n");
            // Display all records where net salary is more than 20000
            Console.WriteLine("Records where net salary is more than 20000:");
            var highSalaryDevelopers = developers.OfType<OnPayroll>().Where(d => d.CalculateSalary() > 20000);
            foreach (OnPayroll developer in highSalaryDevelopers)
            {
                Console.WriteLine(developer.Name + "\t" + developer.CalculateSalary());
                Console.WriteLine("\n");
            }

            Console.WriteLine("\n");
            // Display all records where name contains 'D'
            Console.WriteLine("Records where name contains 'D':");
            var dNameDevelopers = developers.Where(d => d.Name.Contains("D"));
            foreach (Developer developer in dNameDevelopers)
            {
                Console.WriteLine(developer);
                Console.WriteLine("\n");
            }
        }

        static void CreateDeveloper()
        {
            Console.Write("Enter developer ID: ");
            int id = Convert.ToInt32(Console.ReadLine());


            Console.Write("Enter developer name: ");
            string name = Console.ReadLine();

            Console.Write("Enter developer Assigned Project Name: ");
            string project = Console.ReadLine();

            Console.WriteLine("Enter Joining Date...");
            DateTime dateJoing = Convert.ToDateTime(Console.ReadLine());


            Console.WriteLine("Choose type of developer:");
            Console.WriteLine("1. On Contract");
            Console.WriteLine("2. On Payroll");
            Console.Write("Enter your choice: ");
            int choice = Convert.ToInt32(Console.ReadLine());

            if (choice == 1)
            {

                Console.WriteLine("Enter duration in hours");
                int duration = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Enter Charges Per Day");
                decimal chargesPerDay = Convert.ToDecimal(Console.ReadLine());

                developers.Add(new OnContract(id, name, dateJoing, project, duration, chargesPerDay));

            }

            if (choice == 2)
            {
                Console.WriteLine("Enter Department : ");
                string department = Console.ReadLine();

                Console.WriteLine("Enter Manager name : ");
                string mName = Console.ReadLine();

                Console.WriteLine("Enter Basic Salary : ");
                decimal b_Salary = Convert.ToDecimal(Console.ReadLine());

                Console.WriteLine("Enter DA : ");
                decimal da = Convert.ToDecimal(Console.ReadLine());

                Console.WriteLine("Enter HRA : ");
                decimal hra = Convert.ToDecimal(Console.ReadLine());

                Console.WriteLine("Enter LTA : ");
                decimal lta = Convert.ToDecimal(Console.ReadLine());

                Console.WriteLine("Enter PF : ");
                decimal pf = Convert.ToDecimal(Console.ReadLine());

                developers.Add(new OnPayroll(id, name, dateJoing, project, department, mName, b_Salary, da, hra, lta, pf));

            }

           
        }


        static void CalculateTotalSalaryOrCharges()
        {
            if (developers.Count == 0)
            {
                Console.WriteLine("No developers found.");
                Console.WriteLine();
                return;
            }

            Console.WriteLine("Choose calculation type:");
            Console.WriteLine("1.Check  Salary");
            Console.WriteLine("2.Check  Charges");

            Console.Write("Enter your choice: ");
            int choice = Convert.ToInt32(Console.ReadLine());

            if (choice == 1)
            {
                foreach (OnPayroll developer in developers.OfType<OnPayroll>())
                {
                    Console.WriteLine(developer);
                }
            }
            else
            {
                foreach (OnContract developer in developers.OfType<OnContract>())
                {
                    Console.WriteLine(developer);
                }
            }
            Console.WriteLine();
        }



    }

}






