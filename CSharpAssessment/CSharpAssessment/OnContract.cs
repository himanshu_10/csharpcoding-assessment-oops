﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssessment
{
    public class OnContract : Developer
    {
        public int Duration { get; set; }
        public decimal ChargesPerHour { get; set; }

        public OnContract(int id, string name, DateTime jDate, string projAssigned, int duration, decimal chargesPerHour) : base(id, name, jDate, projAssigned)
        {
            Duration = duration;
            ChargesPerHour = chargesPerHour;
        }

        public override decimal CalculateSalary()
        {
            return Duration * ChargesPerHour;
        }

        public override string ToString()
        {
            return $"ID: {Id}\nName: {Name}\nDate of Joining : {JoiningDate}\n Project Assigned : {ProjectAssigned}\nType: On Contract\nDuration: {Duration} days\nCharges Per Hours {ChargesPerHour}\nTotal Charges: {CalculateSalary()}";
        }
    }

}
